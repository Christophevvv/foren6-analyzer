/*
 * This file is part of Foren6, a 6LoWPAN Diagnosis Tool
 * Copyright (C) 2013, CETIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file
 *         6TOP Definitions
 * \author
 *         Christophe Verdonck <Christophe.Verdonck@student.uantwerpen.be>
 */

#include "sixtop_def.h"
#include "stddef.h"

void
init_sixtop_statistics(sixtop_statistics_t * statistics)
{
	statistics->total = 0;
	statistics->add = 0;
	statistics->del = 0;
	statistics->relocate = 0;
	statistics->count = 0;
	statistics->list = 0;
	statistics->signal = 0;
	statistics->clear = 0;
	statistics->responses = 0;
	statistics->confirmations = 0;
}

void
init_sixtop_errors(sixtop_errors_t * errors)
{
	errors->rc_err = 0;
	errors->rc_reset = 0;
	errors->rc_err_version = 0;
	errors->rc_err_sfid = 0;
	errors->rc_err_seqnum = 0;
	errors->rc_err_celllist = 0;
	errors->rc_err_busy = 0;
	errors->rc_err_locked = 0;
}

void
sixtop_statistics_delta(const sixtop_statistics_t* left,
						const sixtop_statistics_t* right,
						sixtop_statistics_delta_t* delta)
{
	if(delta == NULL)
		return;
	if(left == NULL && right == NULL) {
		delta->total = 0;
		delta->add = 0;
		delta->del = 0;
		delta->relocate = 0;
		delta->count = 0;
		delta->list = 0;
		delta->signal = 0;
		delta->clear = 0;
		delta->responses = 0;
		delta->confirmations = 0;
	} else if(left == NULL || right == NULL) {
		delta->total = right ? right->total : left->total;
		delta->add = right ? right->add : left->add;
		delta->del = right ? right->del : left->del;
		delta->relocate = right ? right->relocate : left->relocate;
		delta->count = right ? right->count : left->count;
		delta->list = right ? right->list : left->list;
		delta->signal = right ? right->signal : left->signal;
		delta->clear = right ? right->clear : left->clear;
		delta->responses = right ? right->responses : left->responses;
		delta->confirmations = right ? right->confirmations : left->confirmations;
	} else {
		delta->total = right->total - left->total;
		delta->add = right->add - left->add;
		delta->del = right->del - left->del;
		delta->relocate = right->relocate - left->relocate;
		delta->count = right->count - left->count;
		delta->list = right->list - left->list;
		delta->signal = right->signal - left->signal;
		delta->clear = right->clear - left->clear;
		delta->responses = right->responses - left->responses;
		delta->confirmations = right->confirmations - left->confirmations;		
	}
	delta->has_changed = delta->total || delta->add ||
		delta->del ||
		delta->relocate ||
		delta->count ||
		delta->list ||
		delta->signal ||
		delta->clear ||
		delta->responses ||
		delta->confirmations;
}

void
sixtop_errors_delta(const sixtop_errors_t* left,
					const sixtop_errors_t* right,
					sixtop_errors_delta_t* delta)
{
	if(delta == NULL)
		return;
	if(left == NULL && right == NULL) {
		delta->rc_err = 0;
		delta->rc_reset = 0;
		delta->rc_err_version = 0;
		delta->rc_err_sfid = 0;
		delta->rc_err_seqnum = 0;
		delta->rc_err_celllist = 0;
		delta->rc_err_busy = 0;
		delta->rc_err_locked = 0;		
	} else if(left == NULL || right == NULL) {
		delta->rc_err = right ? right->rc_err : left->rc_err;
		delta->rc_reset = right ? right->rc_reset : left->rc_reset;
		delta->rc_err_version = right ? right->rc_err_version : left->rc_err_version;
		delta->rc_err_sfid = right ? right->rc_err_sfid : left->rc_err_sfid;
		delta->rc_err_seqnum = right ? right->rc_err_seqnum : left->rc_err_seqnum;
		delta->rc_err_celllist = right ? right->rc_err_celllist : left->rc_err_celllist;
		delta->rc_err_busy = right ? right->rc_err_busy : left->rc_err_busy;
		delta->rc_err_locked = right ? right->rc_err_locked : left->rc_err_locked;
	} else {
		delta->rc_err = right->rc_err - left->rc_err;
		delta->rc_reset = right->rc_reset - left->rc_reset;
		delta->rc_err_version = right->rc_err_version - left->rc_err_version;
		delta->rc_err_sfid = right->rc_err_sfid - left->rc_err_sfid;
		delta->rc_err_seqnum = right->rc_err_seqnum - left->rc_err_seqnum;
		delta->rc_err_celllist = right->rc_err_celllist - left->rc_err_celllist;
		delta->rc_err_busy = right->rc_err_busy - left->rc_err_busy;
		delta->rc_err_locked = right->rc_err_locked - left->rc_err_locked;		
	}
	delta->has_changed = delta->rc_err ||
		delta->rc_reset ||
		delta->rc_err_version ||
		delta->rc_err_sfid ||
		delta->rc_err_seqnum ||
		delta->rc_err_celllist ||
		delta->rc_err_busy ||
		delta->rc_err_locked;
}

bool
sixtop_statistics_compare(const sixtop_statistics_t* left,
						  const sixtop_statistics_t* right)
{
	sixtop_statistics_delta_t delta;

	sixtop_statistics_delta(left,right,&delta);
	return delta.has_changed;
}

bool
sixtop_errors_compare(const sixtop_errors_t* left,
					  const sixtop_errors_t* right)
{
	sixtop_errors_delta_t delta;

	sixtop_errors_delta(left,right,&delta);
	return delta.has_changed;
}
